#!/bin/python

## Script for encoding roman numerals for gaming or otherwise purpose
## Also an exercise in non-OO where OO might be normal. 

import sys

#Parameters: in NUMBER (aggreed to be an integer less than 4000.)
#Output: A string representation of NUMBER in unambiguous Roman numerals. 

def NthDigit(oneString, fiveString, tenString, n):
	"Determine the representation of a digit with the passed in properties. integer n >= 0." 
	returnString = ""
	#print("Running NthDigit for n = "+str(n))
	if(n < 4):
		m = n
		while(m > 0):
			returnString +=oneString
			m = m-1
		return returnString
	if(n==4):
		returnString += oneString
		returnString += fiveString
		return returnString
	if(n < 9):
		m = n - 5
		returnString += fiveString
		while(m > 0):
			returnString += oneString
			m = m-1
		return returnString
	if(n == 9):
		returnString += oneString
		returnString += tenString
		return returnString
		
number = int(sys.argv[1])
ones = NthDigit("I", "V", "X", number % 10)
tens = NthDigit("X", "L", "C", (number/10)%10)
hundreds = NthDigit("C", "D", "M", (number/100%10))
thousands = NthDigit("M", "?", "?", (number/1000))
print("The representation of "+sys.argv[1]+" is "+thousands+hundreds+tens+ones+".")

