#!/bin/python

import sys

def main(argv): 
    "Takes a list of flags for coin size and values. Then returns them as a decimal based on the highest coin denomination received."
    assert (len(argv) % 2 == 0)
    platinum = False
    gold = False
    silver = False
    copper = False
    platval = 0
    goldval = 0
    silvval = 0
    coppval = 0

    if(argv.count('p') > 0):
        platinum = True
        platval = argv[argv.index('p')+1]
    if(argv.count('g') > 0):
        gold = True
        goldval = argv[argv.index('g')+1]
    if(argv.count('s') > 0):
        silver = True
        silvval = argv[argv.index('s')+1]
    if(argv.count('c') > 0):
        copper = True
        coppval = argv[argv.index('c')+1]
    
    assert(platinum | gold | silver | copper)
    valstring = " Platinum" if platinum else " Gold" if gold else " Silver" if silver else " Copper" 
    valstring += " Pieces."

    if(platinum):
        retval = float(platval) + .1 * float(goldval) + .01 * float(silvval) + .001 * float(coppval)
    elif(gold):
        retval = float(goldval) + .1 * float(silvval) + .01 * float(coppval)
    elif(silver):
        retval = float(silvval) + .1 * float(coppval)
    else:
        retval = coppval
    return str(retval)+valstring


print main(sys.argv[1:])

