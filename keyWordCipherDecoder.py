#!/bin/python

import sys

baseAlphabet = ['a','b','c','d','e','f','g','h','i','j','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
keyword = sys.argv[1].lower()
message = sys.argv[2].lower()
crazyMessage = ""
sanitycheck = ""

def makeNewAlphabetFromBaseAndKeyword(base, keyword):
    addedKeyword = ""
    returnedAlphabet = list(base)
    for i in keyword:
        for j in returnedAlphabet:
            if(i == j):
                returnedAlphabet.remove(j)
            	break;

        if (i not in addedKeyword):
            addedKeyword += i

    returnedAlphabet = addedKeyword+"".join(returnedAlphabet)
    return returnedAlphabet

def makeEncodedSecretMessageWithKeywordCipher(baseAlphabet, keyedAlphabet, secretMessage):
    outMessage = ""
    for i in secretMessage:
	if(i in baseAlphabet):
        	k=keyedAlphabet[baseAlphabet.index(i)]
        	outMessage += k
	else:
		outMessage += i
    return outMessage

def decodeSecretMessageWithKeywordCipher(baseAlphabet, keyAlphabet, secretMessage):
	decodedMessage = ""
	for i in secretMessage:
		if(i in baseAlphabet):
			k=baseAlphabet[keyAlphabet.index(i)]
			decodedMessage += k
		else:
			decodedMessage += i
	return decodedMessage

def main():
#	print("Using "+sys.argv[1]+" to encode and then decode "+sys.argv[2])
#	print("BASE : "+"".join(baseAlphabet))
	finalAlphabet = makeNewAlphabetFromBaseAndKeyword(baseAlphabet, keyword)
#	print("FINAL: "+finalAlphabet)
#	print("Base  message: ")
#	print(message)
	crazyMessage = makeEncodedSecretMessageWithKeywordCipher(baseAlphabet, finalAlphabet, message)
#	print("Final Message: ")
	print(crazyMessage)	

#	sanityCheck = decodeSecretMessageWithKeywordCipher(baseAlphabet, finalAlphabet, crazyMessage)
#	print("Sanity Check: ")
#	print(sanityCheck)

if __name__ == "__main__":
    main()
